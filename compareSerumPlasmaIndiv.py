#! /usr/bin/python

import re
import os
import sys
import numpy as np
import pandas as pd
import scipy.stats as st
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

def getSerumMatrix():
    df = pd.read_csv('/home/jzhuang@ms.local/NASH/2017AugSerum/NASHNAFLD.congregated.tsv',sep='\t',index_col=0)
    df = df.loc[map(lambda x: not re.search(r'^ERCC-',x),df.index),:]
    df = df.transpose()
    df.index = map(lambda x: x.replace('_','-').split('-')[0], df.index)
    #df = df.loc[:,df.iloc[:-1,:].apply(lambda x: len([i for i in x if i > 1]) > (df.shape[0]-1) * 0.8, axis=0)]

    info = pd.read_csv('/home/jzhuang@ms.local/NASH/2017AugSerum/sampleInfo.tsv',sep='\t',index_col=0)
    info.index = info.index.astype('str')
    info = info[info['uniqFrags'] > 300000]
    info = info[info['disease']=='Control']

    name = df.loc['Description',:]
    df = pd.concat([ df,info.iloc[:,:-1] ],axis=1,join='inner')
    df1 = df.iloc[:,:-4].astype('float')
    grouped = pd.concat([df1,df['PatientName']],join='inner',axis=1).groupby('PatientName')
    table = grouped.agg(np.mean)
    table = table.loc[:,table.apply(lambda x: len([i for i in x if i > 1]) > table.shape[0] * 0.8, axis=0)]
    table.columns = map(lambda x: re.sub(r'\.\d+$','',x),table.columns)
    #info1 = info.iloc[:,:-1]
    #info1.index = info1['PatientName']
    #info1 = info1[~info1.index.duplicated(keep='first')]
    #table = pd.concat([table,info1],join='inner',axis=1)
    #table = table.append(name)
    return table

def getPlasmaMatrix():
    df = pd.read_csv('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/Normal_plasma.congregated.tsv',sep='\t',index_col=0)
    df = df.loc[map(lambda x: not re.search(r'^ERCC-',x),df.index),:]
    df = df.transpose()
    df.index = map(lambda x: x.replace('_','-').split('-')[0], df.index)
    name = df.loc['Description',:]
    df = df.iloc[:-2,:].astype('float')
    grouped0 = df.groupby(df.index)
    df = grouped0.agg(np.mean)
    #df = df.loc[:,df.iloc[:-1,:].apply(lambda x: len([i for i in x if i > 1]) > (df.shape[0]-1) * 0.8, axis=0)]

    info = pd.read_csv('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/plasma.sampleInfo.csv',index_col=0)
    info.index = info.index.astype('str')
    info = info[info['UniqFrags'] > 300000]
    df = pd.concat([ df,info ],axis=1,join='inner')
    df1 = df.iloc[:,:-3].astype('float')
    grouped = pd.concat([df1,df['PatientName']],join='inner',axis=1).groupby('PatientName')
    table = grouped.agg(np.mean)
    table = table.loc[:,table.apply(lambda x: len([i for i in x if i > 1]) > table.shape[0] * 0.8, axis=0)]
    table.columns = map(lambda x: re.sub(r'\.\d+$','',x),table.columns)
    #info1 = info.iloc[:,:-1]
    #info1.index = info1['PatientName']
    #info1 = info1[~info1.index.duplicated(keep='first')]
    #table = pd.concat([table,info1],join='inner',axis=1)
    #table = table.append(name)
    return table


def plotScatter():
    selected_genes = []
    with open('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/nonBlood.csv','rU') as infile:
    #with open('tsGenes','rU') as infile:
        for line in infile:
            a = line.split(',')
            selected_genes.append(a[0])

    #df = pd.read_csv('/home/jzhuang@ms.local/CoExpression/GTEx.tissues.median.tsv',sep='\t',index_col=0)
    #df = df.loc[map(lambda x: x in selected_genes,df['Description']),:]
    plasma = getPlasmaMatrix().transpose()
    serum = getSerumMatrix().transpose() 
    #print plasma
    #print serum

    sns.set(font_scale=1.4)
    from matplotlib.backends.backend_pdf import PdfPages
    pdf = PdfPages('individual_comparison.pdf')
    for patient in serum.columns:
        comb = pd.concat([serum[patient],plasma[patient]],join='outer',axis=1).fillna(0)
        comb.columns = ['serum','plasma']
        comb = comb.loc[map(lambda x: x in selected_genes,comb.index),:]
        comb2 = comb[(comb['serum']>0) & (comb['plasma']>0)].apply(lambda x: np.log10(x+0.1),axis=0)
        comb = comb[(comb['serum']>0) | (comb['plasma']>0)].apply(lambda x: x+0.1,axis=0)
        print comb
        num = comb2.shape[0]
        print comb.shape[0]-num
        pcc = st.pearsonr(comb2['serum'],comb2['plasma'])[0]
        comb.plot.scatter('serum','plasma',loglog=True,figsize=(12,10),s=75,fontsize=15,title=patient+'\nPCC = %s\n%d' % \
                          ('{0:.4f}'.format(pcc),num))
        plt.plot([0.05,1000],[0.05,1000],ls='--',lw=2,color='red')
        #for label,x,y in zip(comb['Description'],comb['serum'],comb['plasma']):
        #    plt.annotate(label,xy=(x,y),xytext=(5,0),textcoords='offset points',ha='left',va='center')
        plt.xlim(0.05,1000)
        plt.ylim(0.05,1000)
        plt.xlabel('Serum TPM')
        plt.ylabel('Plasma TPM')
        plt.savefig(pdf,format='pdf')
        plt.close()
    pdf.close()

def pccDist():
    selected_genes = []
    with open('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/nonBlood.csv','rU') as infile:
        for line in infile:
            a = line.split(',')
            selected_genes.append(a[0])

    plasma = getPlasmaMatrix().transpose()
    serum = getSerumMatrix().transpose() 
    plasma = plasma.loc[map(lambda x: x in selected_genes,plasma.index),:]
    serum = serum.loc[map(lambda x: x in selected_genes,serum.index),:]
    print plasma.shape
    print serum.shape

    PCCs = {}
    for patient in serum.columns:
        #comb = pd.concat([serum[patient],plasma[patient]],join='outer',axis=1).fillna(0)
        comb = pd.concat([serum[patient],plasma[patient]],join='inner',axis=1)
        comb.columns = ['serum','plasma']
        comb = comb[(comb['serum']>0) | (comb['plasma']>0)].apply(lambda x: np.log10(x+0.1),axis=0)
        pcc = st.pearsonr(comb['serum'],comb['plasma'])[0]
        PCCs.update({patient: pcc})

    PCCs2 = {}
    for i in range(plasma.shape[1]):
        j = i+1
        while j < plasma.shape[1]:
            #comb = pd.concat([plasma.iloc[:,i],plasma.iloc[:,j]],join='outer',axis=1).fillna(0)
            comb = pd.concat([plasma.iloc[:,i],plasma.iloc[:,j]],join='inner',axis=1)
            comb.columns = ['p1','p2']
            comb = comb[(comb['p1']>0) | (comb['p2']>0)].apply(lambda x: np.log10(x+0.1),axis=0)
            pcc = st.pearsonr(comb['p1'],comb['p2'])[0]
            PCCs2.update({plasma.columns[i]+' vs '+plasma.columns[j]: pcc})
            j += 1

    PCCs3 = {}
    for i in range(serum.shape[1]):
        j = i+1
        while j < serum.shape[1]:
            #comb = pd.concat([serum.iloc[:,i],serum.iloc[:,j]],join='outer',axis=1).fillna(0)
            comb = pd.concat([serum.iloc[:,i],serum.iloc[:,j]],join='inner',axis=1)
            comb.columns = ['p1','p2']
            comb = comb[(comb['p1']>0) | (comb['p2']>0)].apply(lambda x: np.log10(x+0.1),axis=0)
            pcc = st.pearsonr(comb['p1'],comb['p2'])[0]
            PCCs3.update({plasma.columns[i]+' vs '+plasma.columns[j]: pcc})
            j += 1

    sns.set(font_scale=1.5)
    fig,ax = plt.subplots(figsize=(12,10))
    sns.distplot(PCCs.values(),kde=True,hist=False,rug=True,color='purple',norm_hist=True,ax=ax,label='Plasma VS Serum same person')
    sns.distplot(PCCs2.values(),kde=True,hist=False,rug=False,color='red',norm_hist=True,ax=ax,label='Plasma diff persons')
    #sns.distplot(PCCs3.values(),kde=True,hist=False,rug=False,color='blue',norm_hist=True,ax=ax,label='Serum diff persons')
    plt.legend(loc='best')
    plt.xlabel('Pearson Correlation Coefficient')
    plt.ylabel('Density')
    plt.savefig('PCC_dist.png')
    plt.close()


def plotBox():
    sig = pd.read_csv('Serum_vs_Plasma.DESeq2.sig',header=None)
    plasma = getPlasmaMatrix()
    serum = getSerumMatrix()
    sns.set(font_scale=2)
    from matplotlib.backends.backend_pdf import PdfPages
    pdf = PdfPages('DE_genes_compare.pdf')
    for index,row in sig.iloc[:100,:].iterrows():
        gene = row[0]
        top = pd.DataFrame({'x': np.random.normal(1,0.02,plasma.shape[0]), 'y': plasma[gene]})
        bot = pd.DataFrame({'x': np.random.normal(2,0.02,serum.shape[0]), 'y': serum[gene]})
        comb = pd.concat([top,bot],axis=0)
        comb.plot.scatter('x','y',figsize=(15,12),fontsize=20,s=80)
        plt.boxplot([plasma[gene],serum[gene]],boxprops={'linewidth':2},medianprops={'linewidth':2},whiskerprops={'linewidth':2},capprops={'linewidth':2},sym='')
        #plt.ylim(comb['y'].max()*(-0.1),comb['y'].max()*1.1)
        plt.xticks([1,2],['Plasma','Serum'])
        plt.xlabel('Source')
        plt.ylabel('TPM')
        plt.title('  '.join([gene,str(row[2]),'%.3E' % float(row[1])]))
        plt.savefig(pdf,format='pdf')
        plt.close()
    pdf.close()

def printGenes():
    sig = pd.read_csv('Serum_vs_Plasma.DESeq2.sig',header=None)
    plasma = getPlasmaMatrix()
    serum = getSerumMatrix()
    for index,row in sig.iloc[:100,:].iterrows():
        gene = row[0]
        if np.median(plasma[gene]) < np.median(serum[gene]):
            print gene

def nonBloodCV():
    selected_genes = []
    with open('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/nonBlood.csv','rU') as infile:
        for line in infile:
            a = line.split(',')
            selected_genes.append(a[0])

    plasma = getPlasmaMatrix().transpose()
    serum = getSerumMatrix().transpose() 
    sns.set(font_scale=1.8)
    plasma['CV'] = plasma.apply(lambda x: np.std(x)/np.mean(x),axis=1)
    serum['CV'] = serum.apply(lambda x: np.std(x)/np.mean(x),axis=1)
    #plasma['CV'] = plasma.apply(lambda x: np.log10(np.mean(x)+0.01),axis=1)
    #serum['CV'] = serum.apply(lambda x: np.log10(np.mean(x)+0.01),axis=1)
    comb1 = pd.concat([plasma['CV'],serum['CV']],join='inner',axis=1)
    comb1.columns = ['Plasma CV', 'Serum CV']
    comb1 = comb1.loc[map(lambda x: x in selected_genes,comb1.index),:]
    print comb1.shape
    #comb1.plot('Serum CV','Plasma CV',kind='scatter',figsize=(12,10),s=80,fontsize=15,title='Serum VS Plasma CV comparison')
    #comb1.plot('Serum CV','Plasma CV',kind='hexbin',figsize=(12,10),fontsize=15,title='Serum VS Plasma CV comparison',gridsize=50)
    g = sns.jointplot(x='Serum CV',y='Plasma CV',data=comb1,kind='kde',size=12,xlim=(0,1.2),ylim=(0,1.2))
    #g = sns.jointplot(x='Serum Mean',y='Plasma Mean',data=comb1,kind='kde',size=12,xlim=(0,2.5),ylim=(0,2.5))
    g.plot_joint(plt.scatter, s=40, c='w', marker='+',lw=1)
    g.ax_joint.collections[0].set_alpha(0)
    g.ax_joint.plot([-1,100],[-1,100],ls='--',lw=2,color='red')
    plt.savefig('NonBlood_CV_comparison.png')
    plt.close()
    

def main():
    #getPlasmaMatrix().transpose().to_csv('plasmaNormals.csv')
    #getSerumMatrix().transpose().to_csv('serumNormals.csv')
    plotScatter()
    #plotBox()
    #printGenes()
    #nonBloodCV()
    #pccDist()
    

if __name__=='__main__':
    main()
