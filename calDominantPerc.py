#! /usr/bin/python

import re
import os
import sys
import glob
import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from check_const_genes1 import getConstGenes
from check_const_genes1 import getMatrix
from check_const_genes1 import getMatrix2

def getVmat(chainType):
    files = glob.glob('*.IGgenes.readCounts')
    data = {'1st':{}, '2nd':{}, 'total':{}}
    for fn in files:
        with open(fn,'rU') as infile:
            total = 0
            m = 0
            n = 0
            sample = fn.replace('.IGgenes.readCounts','')
            if re.search(r'-IP0',sample):
                sample = 'MM-'+sample.split('-')[0]

            for line in infile:
                a = line.split()
                if re.search(r'^%s' % chainType,a[0]):
                    a[2] = int(a[2])
                    total += a[2]
                    if a[2] > m:
                        m = a[2]
                    elif a[2] > n:
                        n = a[2]
            data['1st'].update({sample: m})
            data['2nd'].update({sample: n})
            data['total'].update({sample: total})

    df = pd.DataFrame.from_dict(data)
    df['ratio1'] = df['1st']/df['total']
    df['ratio2'] = df['1st']/df['2nd']
    return df

def getVmat1(Vgene):
    files = glob.glob('*.IGgenes.readCounts')
    data = {'Vgene':{}, 'total':{}}
    for fn in files:
        with open(fn,'rU') as infile:
            total = 0
            m = 0
            sample = fn.replace('.IGgenes.readCounts','')
            if re.search(r'-IP0',sample):
                sample = 'MM-'+sample.split('-')[0]

            for line in infile:
                a = line.split()
                if re.search(r'^%s' % Vgene[:4],a[0]):
                    a[2] = int(a[2])
                    total += a[2]
                    if Vgene == a[0]:
                        m += a[2]
            data['Vgene'].update({sample: m})
            data['total'].update({sample: total})

    df = pd.DataFrame.from_dict(data)
    df['ratio'] = df['Vgene']/df['total']
    return df

def getCmat():
    files = glob.glob('*.IGgenes.readCounts')
    data = {'Heavy':{}, 'Lambda':{}, 'Kappa':{}}
    for fn in files:
        with open(fn,'rU') as infile:
            h = 0
            l = 0
            k = 0
            sample = fn.replace('.IGgenes.readCounts','')
            if re.search(r'-IP0',sample):
                sample = 'MM-'+sample.split('-')[0]

            for line in infile:
                a = line.split()
                if re.search(r'^IGKC',a[0]):
                    k += int(a[2])
                elif re.search(r'^IGLC',a[0]):
                    l += int(a[2])
                elif re.search(r'^IGH[AMGE]',a[0]):
                    h += int(a[2])

            data['Heavy'].update({sample: h})
            data['Lambda'].update({sample: l})
            data['Kappa'].update({sample: k})

    df = pd.DataFrame.from_dict(data)
    df = df[(df['Lambda']>30) & (df['Kappa']>30)]
    df['ratio1'] = df['Kappa']/df['Lambda']
    return df
    
def getConstTPM():
    IGTRgeneList = getConstGenes()
    df1 = getMatrix(IGTRgeneList)
    df2 = getMatrix2(IGTRgeneList)
    princeC = map(lambda x: '-'.join(x.split('-')[:2]), glob.glob('Prince*.readCounts'))

    expr = pd.read_csv('/home/jzhuang@ms.local/NASH/2017JunPrince/Prince_Jun.salmon.ENRcongregated1.tsv',sep='\t',index_col=0)
    expr = expr.loc[map(lambda x: x in IGTRgeneList,expr['Description']),:]
    df3 = pd.concat([expr.loc[:,map(lambda x: x in princeC,expr.columns)],expr['Description']],join='inner',axis=1)
    df3.index = df3['Description']
    df3 = df3.drop('Description',axis=1).transpose()
    df3 = df3.loc[:,map(lambda x: not re.search(r'^IG',x)==None,df3.columns)]
    df3['K/L ratio'] = df3.apply(lambda x: x['IGKC']/sum(x[['IGLC1','IGLC2','IGLC3','IGLC7']]),axis=1)

    df = pd.concat([df2,df1,df3],join='inner',axis=0)
    return df
    

def plotBox():
    df = getVmat('IGLV')
    #df = getCmat()
    #df = getConstTPM()
    df = df.loc[map(lambda x: not re.search(r'-dep',x),df.index),:]
    df['diagnosis'] = ['CTRL']*7 + ['iSpecimen']*4 + ['MM01']*15 + ['MM02']*19 + ['Prince_ctrl']*27

    print df
    grouped = df.groupby('diagnosis')
    boxes = []
    keys = []
    dots = []
    for key,group in grouped:
        boxes.append(list(group['ratio1']))
        keys.append(key)
        dots.append(pd.DataFrame({'Disease':np.random.normal(len(keys),0.05,group.shape[0]), 
                                  'Ratio':list(group['ratio1'])}
                             ))
    pd.concat(dots,axis=0).plot.scatter('Disease','Ratio',figsize=(15,12),fontsize=18,s=80)
    plt.boxplot(boxes,boxprops={'linewidth':2},medianprops={'linewidth':2},whiskerprops={'linewidth':2},capprops={'linewidth':2},sym='')
    plt.xticks([1,2,3,4,5],keys)
    plt.xlabel('Disease',fontsize=20)
    plt.ylabel('Ratio',fontsize=20)
    plt.title('Dominant IGLV gene ratio',fontsize=20)
    #plt.title('Kappa/Lambda ratio',fontsize=20)
    plt.savefig('IGLV.ratio1.png')
    #plt.savefig('KappaLambda.TPM.ratio.png')

def plotLineKLratio():
    IGTRgeneList = getConstGenes()
    df = pd.read_csv('MM02.congregated.tsv',sep='\t',index_col=0).iloc[:,:-1]
    df = df.loc[map(lambda x: x in IGTRgeneList,df['Description']),:]
    df.index = df['Description']
    df = df.drop('Description',axis=1).transpose()
    df = df.astype('float')
    df = df.loc[:,map(lambda x: not re.search(r'^IG',x)==None,df.columns)]
    df['ratio1'] = df.apply(lambda x: x['IGKC']/sum(x[['IGLC1','IGLC2','IGLC3','IGLC7']]),axis=1)
    df['days'] = range(-2,16)
    print df
    df[['ratio1','days']].plot('days','ratio1',kind='line',title='Kappa/Lambda ratio time course',legend=False,lw=2.5)
    plt.savefig('MM02.KLratio.timecourse.png')
    
def plotLine():
    df = getVmat('IGHV')
    df = df.loc[map(lambda x: not re.search(r'-dep',x),df.index),:]

    #df = df.iloc[11:26,:]
    #df['days'] = range(-2,11)+[12,13]
    df = df.iloc[27:45,:]
    df['days'] = range(-2,16)
    print df
    df[['ratio1','days']].plot('days','ratio1',kind='line',title='Dominant IGHV gene frac time course',legend=False,lw=2.5)
    plt.savefig('MM02.IGHV.ratio1.timecourse.png')

def plotLine1(Vgene):
    df = getVmat1(Vgene)
    df = df.iloc[11:26,:]
    df['days'] = range(-2,11)+[12,13]
    print df
    df[['ratio','days']].plot('days','ratio',kind='line',title='Dominant gene %s frac time course' % Vgene,legend=False,lw=2.5)
    plt.savefig('%s.ratio.timecourse.png' % Vgene)

    
def main():
    pd.options.display.max_rows = 100
    #df = getVmat('IGHV')
    #df = df.loc[map(lambda x: not re.search(r'-dep',x),df.index),:]
    #print df
    #plotBox()
    plotLine()
    #plotLine1('IGHV1-69')
    #getConstTPM()
    #plotLineKLratio()


if __name__=='__main__':
    main()
