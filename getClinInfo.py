#! /usr/bin/python

import re
import os
import psycopg2
import numpy as np
import pandas as pd

def connect():
    #su postgres
    #password: Welcome@ms
    #cd /usr/local/pgsql/data
    #psql
    conn = psycopg2.connect(dbname="test_db",user="postgres")
    return conn

def getClinicInfo(tbl,conn):
    df = pd.read_csv(tbl,sep='\t',index_col=0)
    alqIDs = ','.join(map(lambda x: x.split('-')[0],df.columns[:-2]))
    clinInfo = {'PatientID':{},'Disease':{},'PathFibrosis':{}}
    cur = conn.cursor()
    cur.execute("SELECT tbaliquot.\"AliquotID\",tbclinicalhx.\"PatientID\",tbclinicalhx.\"Disease\",tbclinicalhx.\"PathFibrosis\" FROM tbspecimeninfo INNER JOIN tbaliquot ON (tbspecimeninfo.specimenindexid = tbaliquot.\"Parent\") INNER JOIN tbclinicalhx ON (tbspecimeninfo.caseid = tbclinicalhx.\"CaseID\") WHERE tbaliquot.\"AliquotID\" IN (%s)" % (alqIDs))
    rows = cur.fetchall()
    for row in rows:
        aliquot = str(row[0]).replace('.0','')
        clinInfo['PatientID'].update({aliquot: str(row[1]).replace('.0','')})
        clinInfo['Disease'].update({aliquot: str(row[2])})
        clinInfo['PathFibrosis'].update({aliquot: str(row[3])})
    cur.close()
    del cur
    return pd.DataFrame.from_dict(clinInfo)

def getClinicInfo1(tbl):
    df = pd.read_csv(tbl,sep='\t',index_col=0)
    alqIDs = map(lambda x: x.split('-')[0],df.columns[:-2])
    info = pd.read_csv('tbAliquotQuery_090117.csv',index_col=0,usecols=['AliquotID','PatientName','Disease','PathFibrosis'])
    info = info.loc[map(lambda x: str(x) in alqIDs, info.index),:]
    info = info[~info.index.duplicated(keep='first')]
    print info
    return info


def main():
    conn = connect()
    info = getClinicInfo('PDserum.congregated.tsv',conn)
    print info
    #info[['PatientName','Disease']].to_csv('plasma.sampleInfo.csv')
    

if __name__=='__main__':
    main()
