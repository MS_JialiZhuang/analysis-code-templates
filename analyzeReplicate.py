#! /usr/bin/python

import re
import os
import sys
import glob
import json
import numpy as np
import pandas as pd
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn import decomposition
from compareSerumPlasmaIndiv import getPlasmaMatrix
from compareSerumPlasmaIndiv import getSerumMatrix
import seaborn as sns

def plotPCA():
    selected_genes = []
    with open('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/nonBlood.csv','rU') as infile:
        for line in infile:
            a = line.split(',')
            selected_genes.append(a[0])

    df = pd.read_csv('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/Normal_plasma.congregated.tsv',sep='\t',index_col=0).iloc[:,:-1]
    df = df.loc[map(lambda x: not re.search(r'^ERCC-',x),df.index),:]
    df = df.transpose()
    df = df.loc[:,df.iloc[:-1,:].apply(lambda x: len([i for i in x if i > 1]) > (df.shape[0]-1) * 0.8, axis=0)]
    df.iloc[:-1,:] = preprocessing.StandardScaler().fit_transform(df.iloc[:-1,:])
    pca = decomposition.PCA()
    comp = pd.DataFrame(pca.fit_transform(df.iloc[:-1,:]),index=df.index[:-1])
    comp.columns = map(lambda x: 'PC'+str(x+1),comp.columns)

    info = pd.read_csv('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/plasma.repInfo.csv',sep='\t',index_col=0)
    info = info[['Pool','RNA_ID','LP_Set']]
    info.loc[:,'RNA_ID'] = map(lambda x: x.split('_')[0],info['RNA_ID'])
    comp = pd.concat([comp,info],join='inner',axis=1)
    print comp

    #colors = {1: 'red', 2: 'blue'}
    #colors = {'LG032': 'red', 'JRP105_Set1': 'blue', 'JRP105_Set2': 'orange'}
    colors = {'IP029':'red', 'VH099':'blue', 'AI028':'orange', 'IP027':'black', 'IP028':'purple'}
    fig, ax = plt.subplots(figsize=(12,10))
    sns.set(font_scale=1.6)
    grouped = comp.groupby('RNA_ID')
    for key,group in grouped:
        group.plot.scatter(x=0,y=1,alpha=0.8,color=colors[key],label=key,s=80,ax=ax,fontsize=18)
    #for label,x,y in zip(comp.index,comp['PC1'],comp['PC2']):
    #    ax.annotate(re.sub(r'-Pt\d\d\d','',label),xy=(x,y),xytext=(4.5,0),textcoords='offset points',ha='left',va='center')
    ax.set_xlabel('PC1',fontsize=18)
    ax.set_ylabel('PC2',fontsize=18)
    plt.legend(fontsize=18)
    plt.savefig('repPlasma.RNA_ID.png')


def getStats(f):
    selected_genes = []
    with open('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/nonBlood.csv','rU') as infile:
        for line in infile:
            a = line.split(',')
            selected_genes.append(a[0])

    uniqFrags = {}
    statsFiles = glob.glob('/mnt/nfs/analysis/170910_JRP105Run1/*.stats.json')
    statsFiles += glob.glob('/mnt/nfs/analysis/170912_JRP105Run2/*.stats.json')
    for fn in statsFiles:
        with open(fn,'rU') as jfile:
            sample = os.path.basename(fn).replace('.stats.json','')
            sample = '-'.join(sample.split('-')[:2] + [sample.split('-')[-1]])
            data = json.load(jfile)
            uniqFrags.update({sample: int(data["readStats"]["Uniq_frags"])})

    df = pd.read_csv('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/Normal_plasma.congregated.tsv',sep='\t',index_col=0).iloc[:,:-1]
    df.index = map(lambda x: re.sub(r'\.\d+$','',x),df.index)
    df = df.loc[map(lambda x: not re.search(r'^ERCC-',x),df.index),:]
    df = df.loc[map(lambda x: x in selected_genes,df.index),:]
    df = df.transpose()

    info = pd.read_csv('/home/jzhuang@ms.local/CoExpression/Serum_vs_Plasma/plasma.repInfo.csv',sep='\t',index_col=0)
    info = info[['Pool','RNA_ID','LP_Set']]
    info.loc[:,'RNA_ID'] = map(lambda x: x.split('_')[0],info['RNA_ID'])
    info = pd.concat([info,pd.Series(uniqFrags,name='uniqFrags')],join='inner',axis=1)
    df = pd.concat([df,info],join='inner',axis=1)
    df.loc[:,'uniqFrags'] = df.loc[:,'uniqFrags']/1000000.0
    nbGenes = {}
    df_arrays = []
    colMaps = {'LG032':'blue','JRP105_Set2':'red','JRP105_Set1':'orange'}
    i = 1
    for key,group in df.groupby(f):
        group1 = group.iloc[:,:-4]
        num = group1.loc[:,group1.apply(lambda x: len([i for i in x if i > 1]) > group.shape[0] * 0.8, axis=0)].shape[1]
        nbGenes[key] = num
        df_arrays.append(pd.DataFrame({'x': np.random.normal(i,0.02,group.shape[0]),'y': group['uniqFrags'],'LP': group['LP_Set']}))
        i += 1

    print nbGenes
    df_uniqFrag = pd.concat(df_arrays,join='inner',axis=0)
    print df_uniqFrag
    sns.set(font_scale=1.6)
    fig,ax = plt.subplots(figsize=(12,10))
    bp = df.boxplot(column='uniqFrags',by=f,boxprops={'linewidth':2},medianprops={'linewidth':2},whiskerprops={'linewidth':2},capprops={'linewidth':2},sym='',ax=ax,return_type='dict')
    [[item.set_color('blue') for item in bp[key]['boxes']] for key in bp.keys()]
    [[item.set_color('blue') for item in bp[key]['whiskers']] for key in bp.keys()]
    [[item.set_color('red') for item in bp[key]['medians']] for key in bp.keys()]
    
    for key,group in df_uniqFrag.groupby('LP'):
        group.plot.scatter('x','y',c=colMaps[key],s=80,title='Unique fragments comparison',ax=ax)
    #df_uniqFrag.plot.scatter('x','y',c='blue',s=80,title='Unique fragments comparison',ax=ax)
    plt.xlabel(f,fontsize=18)
    plt.ylabel('Number of Unique Fragments (Million)',fontsize=18)
    plt.ylim(0,7)
    plt.savefig('uniqFrags_by_RNAextract.png')
    plt.close()    


def main():
    #plotPCA()
    getStats('RNA_ID')


if __name__=='__main__':
    main()
